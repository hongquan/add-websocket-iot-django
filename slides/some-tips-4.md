

```py
class CardReaderConsumer(AsyncWebsocketConsumer):
	def get_redis(self) -> aioredis.Redis:
		return self.channel_layer.connection(0)

	async def set_device_online(self, device: CardReader, status: bool):
		async with self.get_redis() as conn:
			key = device.key_is_online
			if status:
				await conn.setex(key, settings.DURATION_CACHE_ONLINE_STATUS, 1)
			else:
				await conn.delete(key)

	async def receive(self, text_data=None, bytes_data=None):
		# Do some custom authentication here
		await self.set_device_online(device, True)
```