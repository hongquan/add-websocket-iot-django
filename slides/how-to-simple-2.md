## How to


- Add _routing.py_ file next to familiar _urls.py_:

	```py
	from channels.auth import AuthMiddlewareStack
	from channels.routing import ProtocolTypeRouter, URLRouter
	import wsapi.routing    # Our module

	application = ProtocolTypeRouter({
	    'websocket': AuthMiddlewareStack(
	        URLRouter(wsapi.routing.websocket_urlpatterns)
	    ),
	})
	```