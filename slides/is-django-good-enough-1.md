## Is Django good enough


- Django is traditionally a synchronous web framework:

    + Blocking I/O

    + Concurency is archieved via multi-threading, multi-processing (by WSGI servers like [gunicorn](https://gunicorn.org/)).

- But that is just one well-known side of Django.

- You can write asyncio code, with `async/await` syntax, in Django.