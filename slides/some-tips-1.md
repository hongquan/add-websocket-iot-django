## Some tips


### Tracking which devies are online

- Doing so by keeping a list of alive "consumers" (a notion in Django Channels)

- Based on facts:
	- For each open connection with device, our Django app creates a "consumer", with unique "channel name".
	- That consumer is destroyed when the connection is closed/lost.