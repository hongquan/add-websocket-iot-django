


My approach is to use Redis:

	- Lightweight
	- Reduce pressure on Postgres
	- Exclude from backup.
	- Auto-expired (related to next issue)