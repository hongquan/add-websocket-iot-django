## How to

- Add _asgi.py_ file next to familiar _wsgi.py_

	```python
	import django
	from channels.routing import get_default_application

	django.setup()
	application = get_default_application()
	```

- No need to change `./manage runserver`
- For production, run the `asgi.py` with an ASGI server.