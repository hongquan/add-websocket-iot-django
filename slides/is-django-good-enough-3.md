## Is Django good enough


- Don't have to rewrite code to `asyncio`. We maintain 2 API set:

	+ Traditional, synchronous RESTful API.
	+ Asynchronous WebSocket API.