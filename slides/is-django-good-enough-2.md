## Is Django good enough


- Official [Django Channels](https://github.com/django/channels) plugin.

- [ASGI](https://asgi.readthedocs.io) spec (adopted by many other Python frameworks).

- Better structured, separate of concern, than some NodeJS and Go framework (as evaluated by a friend).