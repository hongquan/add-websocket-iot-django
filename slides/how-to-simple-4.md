## How to


Our setup:


- Serve WebSocket API
	```sh
	$ daphne project.asgi:application
	```

- Serve HTTP API and all other HTTP endpoints
	```sh
	$ gunicorn project.wsgi
	```

(This separation is not neccessary, just our tactics of load balancing)