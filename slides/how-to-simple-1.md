## How to


- No database migration 🍵
- Add [`channels`](https://pypi.org/project/channels/) as dependency

	```sh
	poetry add channels
	```

- Add `channels` to `INSTALLED_APPS`

	```python
	INSTALLED_APPS = [
		...
		'channels',
		...
	]
	```

- Write code for _consumers_ (equivalent to _views_).