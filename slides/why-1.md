## Why WebSocket


In the most common use cases, IoT devices only send data each several tens of seconds, a keep-alive connection like WebSocket is not necessary.

But...

We want to experiment an Wifi mesh network of devices, where the root device has to talk with server a lot.
